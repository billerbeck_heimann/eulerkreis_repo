import java.util.ArrayList;

public class EulerKreisFinder {

  public static ArrayList findePfad(Verknuepfung v) throws Exception
  {
    ArrayList pfad = new ArrayList();
    int pfadPosition = findeStartpunkt(v, pfad);
    ArrayList teilpfad = new ArrayList();
    teilpfad.add(pfad.get(0));
    
    if (pfadPosition == -1)
    {
      throw new Exception("Kein Eulerkreis");
    }
    
    while (true)
    {
      int nachfolger = findeNachfolger(v, (int) teilpfad.get(teilpfad.size()- 1));
      if (nachfolger == -1)
      {
        if (teilpfad.get(0) == teilpfad.get(teilpfad.size() - 1))
        {
          teilpfad.remove(teilpfad.size() - 1);
          pfad.addAll(pfadPosition, teilpfad);
          pfadPosition = findeStartpunkt(v, pfad);

          // Erfolgreiher Abbruch
          if (v.getKanten() == 0)
          {
              break;
          }

          if (pfadPosition == -1)
          {
            throw new Exception("Kein Eulerkreis");
          }

            teilpfad.clear();
            teilpfad.add(pfad.get(pfadPosition));
        }
        else
        {
          throw new Exception("Kein Eulerkreis");
        }
      }
      else
      {
        streiche(v, nachfolger, (int) teilpfad.get(teilpfad.size() - 1));
        teilpfad.add(nachfolger);
      }
      
    }
    
    return pfad;
  }

  private static int findeStartpunkt(Verknuepfung v, ArrayList pfad)
  {
    int startpunkt = -1;

    for (int i = 0; i < v.getKnoten(); i++)
    {
      if(v.getKantenProKnoten(i) > 0)
      {
          if (pfad.isEmpty())
          {
              startpunkt = 0;
              pfad.add(i);
              break;
          }
          else {
              int indexO = pfad.indexOf(i);
              if (indexO >= 0) {
                  startpunkt = indexO;
                  break;
              }
          }

      }
    }
    
    return startpunkt;
  }

  private static int findeNachfolger(Verknuepfung v, int iKnoten)
  {
    int indexNachfolger = -1;
    for (int i = 0; i < v.getKnoten(); i++)
    {
      if (v.isVerbunden(iKnoten, i))
      {
        indexNachfolger = i;
        break;
      }
    }
    
    return indexNachfolger;
  }

  private static void streiche(Verknuepfung v, int iKnoten, int iVorgaenger)
  {
    v.decrementKantenProKnoten(iKnoten);
    v.decrementKantenProKnoten(iVorgaenger);
    v.streicheVerknuepfung(iKnoten, iVorgaenger);
    v.setKanten(v.getKanten() - 1);
  }
}
