import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.canvas.*;
import javafx.scene.paint.Color; 
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.control.Labeled;
import javafx.event.*;
import java.util.ArrayList;
 
/**
 *
 * Beschreibung
 * Diese Klasse wird fuer die Grafische Darstellung von einem Knotensystem verwendet.
 * Es wird ein Eulerscher Graph dargestellt
 *
 * @version 1.1 vom 28.01.2022
 * @Jannik Heimann
 *
 */

public class Output extends Application  {
  // Finals / Makros
  private static final int INPUTWINDOW_SIZE_X = 400;
  private static final int INPUTWINDOW_SIZE_Y = 100;
  private static final int DRAWWINDOW_SIZE_X = 400;
  private static final int DRAWWINDOW_SIZE_Y = 400;
  private static final int PKT_SIZE = 10;
  
  // Anfang Attribute
  private static Controller controller;
  // Ende Attribute
  
  // Anfang Methoden
  public Output()
  {
    
  }
  
  public void startGUI(Controller c) {
    controller = c;
    launch();
  } // end of startGUI
  
  public void start(Stage inputStage) { 
    GridPane inputPane = new GridPane();
    Scene inputScene = new Scene(inputPane, INPUTWINDOW_SIZE_X, INPUTWINDOW_SIZE_Y);
    
    controller.requestKnots();
    // Anfang Komponenten
    
    // Label - Eingabe Aufforderung:
    Label l_infoText1 = new Label("Bitte geben Sie im unteren Feld die Anzahl der Knoten ein:");
    
    // Textfield - Nutzereingabe
    TextField tf_anz_knoten = new TextField("0");
    
    // Button - Bestaetigung der Eingabe
    Button b_ok = new Button("Bestaetigen");
    b_ok.setOnAction(new EventHandler<ActionEvent>() {
    @Override 
    public void handle(ActionEvent e) {
    int value = 0;
    
    try {
    value = Integer.parseInt(tf_anz_knoten.getText()); // Textfeld Zahl auslesen
    controller.setKnoten(value);         // und an Daten Klassen ueber Controller schicken   
    inputStage.close();
    showAdjustmatrixWindow();  
    } catch(NumberFormatException err) {
    System.out.println("Fehler bitte geben Sie nur ganze Zahlen > 0 ein");  
    }  // end of try
    
    }
    });
    
    // Anordnung im GridPane
    inputPane.add(l_infoText1, 0,0);
    inputPane.add(tf_anz_knoten, 0, 1);
    inputPane.add(b_ok, 0, 2);
    // Ende Komponenten
    
    inputStage.setOnCloseRequest(e -> System.exit(0));
    inputStage.setTitle("Input Eulerkreis");
    inputStage.setScene(inputScene);
    inputStage.show();
  } // end of public Output
  
  // Anfang Methoden 
  
  private void showAdjustmatrixWindow()
  {
    int knots = controller.requestKnots();                           //Knoten anfordern
    boolean[][] verbindungen = new boolean[knots][knots];            // Array fuer Verbindungen erstellen
    int window_x = 400;                       
    int window_y = (knots * 35) + 50;
    
    Stage matrixStage = new Stage();
    GridPane generalPane = new GridPane();
    GridPane matrixPane = new GridPane();
    Scene matrixScene = new Scene(generalPane, window_x, window_y); 
    
    // Anfang Komponenten
    // Label - Ueberschrift / Info
    Label l_ueberschrift = new Label("Bitte klicken Sie die entsprechenden Verbindungen an \n1 = ausgewaehlt, 0 = nicht ausgewaehlt");
    
    // Buttons - Verbindungen in Matrix Form
    MatrixButton[][] matrixButtons = new MatrixButton[knots][knots];
    for (int f = 0; f < knots; f++ ) {       
      for (int g = 0; g < knots; g++ ) {
        matrixButtons[f][g] = new MatrixButton(f, g);                                // Button erstellen
        matrixButtons[f][g].setText("0");
        
        // Nur Buttons, welche nicht auf einer diagonalen sind, sollen einen Eventhaendler haben
        if (f != g)
        {
          matrixButtons[f][g].setOnAction(new EventHandler<ActionEvent>() {  // Evethandler hinzufuegen
          @Override 
          public void handle(ActionEvent e) {
          //// Gegenueberliegenden Button ebenfalls auswaehlen ?
          MatrixButton m_button = (MatrixButton) e.getSource();
          
          if (m_button.getText().equals("0"))
          {
          verbindungen[m_button.getRow()][m_button.getColumn()] = true;
          m_button.setText("1");
          verbindungen[m_button.getColumn()][m_button.getRow()] = true; 
          m_button.getPartnerButton().setText("1");
          }
          else
          {
          verbindungen[m_button.getRow()][m_button.getColumn()] = false; 
          m_button.setText("0");       
          verbindungen[m_button.getColumn()][m_button.getRow()] = false;  
          m_button.getPartnerButton().setText("0");
          }
          
          // Verbindung abschliessend aktualisieren
          controller.setVerbindungen(verbindungen);
          }
          });  
        }
        matrixPane.add(matrixButtons[f][g], f, g);                         // Button auf GUI anordnen
      } // end of for
    } // end of for
    
    // MatrixButtons die jeweiligen Partner zuweisen
    for (int f = 0; f < knots; f++) {
      for (int g = 0; g < knots; g++) {
        if (f != g)
        {
          matrixButtons[f][g].setPartnerButton(matrixButtons[g][f]);  
        }
      } // end of for
    } // end of for
    
    // Button - bestaetigen
    Button b_ok = new Button("Bestaetigen");
    b_ok.setOnAction(new EventHandler<ActionEvent>() {  // Evethandler hinzufuegen
    @Override 
    public void handle(ActionEvent e) {
    matrixStage.close();
    showEulerkreis();
    }
    });
    
    // Button zurueck
    Button b_back = new Button("Zurueck");
    b_back.setOnAction(new EventHandler<ActionEvent>() {  // Evethandler hinzufuegen
    @Override 
    public void handle(ActionEvent e) {
    matrixStage.close();
    start(new Stage());                     //// Ok so, oder arbeitspeicher fresser?
    }
    });
    
    // HBox for Bottom Buttons
    HBox hb_bottom = new HBox();
    hb_bottom.getChildren().addAll(b_ok, b_back);
    
    // Anordnung im generalPane
    generalPane.add(l_ueberschrift, 0, 0);
    generalPane.add(matrixPane, 0, 1);
    generalPane.add(hb_bottom, 0, 2);
    // Ende Komponenten
    
    matrixStage.setOnCloseRequest(e -> System.exit(0));
    matrixStage.setTitle("Adjustmatrix");
    matrixStage.setScene(matrixScene);
    matrixStage.show(); 
  }
  
  private void showEulerkreis()
  {
    Stage drawStage = new Stage();
    GridPane root = new GridPane();
    Scene scene = new Scene(root, DRAWWINDOW_SIZE_X, DRAWWINDOW_SIZE_Y);
    ArrayList<Integer> route = new ArrayList<Integer>();
    Punkt[] pkte = new Punkt[1];
    
    // Flaeche fuer Darstellung
    Canvas canvas = new Canvas(DRAWWINDOW_SIZE_X, DRAWWINDOW_SIZE_Y);
    GraphicsContext gc = canvas.getGraphicsContext2D();
    gc.setFill(Color.GRAY);
    gc.fillRect(0, 0, DRAWWINDOW_SIZE_X, DRAWWINDOW_SIZE_Y);
    root.add(canvas, 0, 0);
    
    // Punkte fuer entsprechende Knoten erstellen
    pkte = printKnots(gc, controller.requestKnots());
    
    // Route anfordern
    try {
      route = controller.getPfad();
      
      // Zeichnen der Punkte und der Route
      printConnections(gc, pkte, route);
      
      // Darstellung des Fensters
      drawStage.setOnCloseRequest(e -> System.exit(0));
      drawStage.setTitle("Darstellung Eulerkreis");
      drawStage.setScene(scene);
      drawStage.show();
    } catch(Exception e) {
      showAdjustmatrixWindow(); 
      errorWindow(e.getMessage()); 
    } 
  }
  
  private void errorWindow(String err_msg)
  {
    Stage err_stg = new Stage();
    GridPane err_pane = new GridPane();
    Scene err_scene = new Scene(err_pane, 200, 100);  
    
    //Label
    Label l_err = new Label("Fehler : " + err_msg);
    err_pane.add(l_err, 0, 0);
    
    //OK Button
    Button b_ok = new Button("Ok");
    err_pane.add(b_ok, 0, 1);
    b_ok.setOnAction(new EventHandler<ActionEvent>() {  // Evethandler hinzufuegen
    @Override 
    public void handle(ActionEvent e) {
    err_stg.close();
    }
    });
    
    err_stg.setOnCloseRequest(e -> System.exit(0));
    err_stg.setTitle("Fehlermeldung");
    err_stg.setScene(err_scene);
    err_stg.show(); 
  }
  
  private void printConnections(GraphicsContext gc, Punkt[] pkte, ArrayList<Integer> route)
  {
    int anz_lines = route.size();
    gc.setLineWidth(1);
    
    for (int f = 0; f < anz_lines - 1; f++) {
      Color cl = new Color(1 - (float) (f / (float) anz_lines), 0.5 - (float) (f / ( (float) anz_lines * 2)), 0.25 - (f / (anz_lines * 4)), 1f);
      gc.setStroke(cl);
      Punkt vorgaenger = pkte[route.get(f)];
      Punkt nachgaenger = pkte[route.get(f + 1)];
      gc.strokeLine(vorgaenger.getX(), vorgaenger.getY(), nachgaenger.getX(), nachgaenger.getY());
    } // end of for
  }  
  
  private Punkt[] printKnots(GraphicsContext gc, int knots)
  {
    Punkt[] pkte = new Punkt[knots];
    double x = 0.;
    double y = 0.;
    double r = 5.;
    double h = 0.;
    double h_max = 0.;  
    
    if (DRAWWINDOW_SIZE_X > DRAWWINDOW_SIZE_Y)
    {
      h_max = DRAWWINDOW_SIZE_Y / 2;
    }
    else
    {
      h_max = DRAWWINDOW_SIZE_X / 2;
    }
    
    for (int f = 0; f < knots; f++) {
      h = h_max - 5;
      x = Math.sin(((double) f / knots) * 2 * Math.PI) * h + DRAWWINDOW_SIZE_X / 2;
      y = Math.cos(((double) f / knots) * 2 * Math.PI) * h + DRAWWINDOW_SIZE_Y / 2;
      
      pkte[f] = new Punkt(x, y, r);
      pkte[f].print(gc);
    } // end of for
    
    return pkte;
  }
  
  public void setController(Controller c)
  {
    this.controller = c;
  }                                                                                                                                              
  // Ende Methoden
} // end of class Output
    
