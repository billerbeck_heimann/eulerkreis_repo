public class Verknuepfung
{
  private int nKnoten;
  private int nKanten;
  private int kantenProKnoten[];
  boolean verbindungen[][];
  
  public Verknuepfung(int nKnoten, int nKanten, int[] kantenProKnoten, boolean[][] verbindungen)
  {
    this.nKnoten = nKnoten;
    this.nKanten = nKanten;
    this.kantenProKnoten = kantenProKnoten;
    this.verbindungen = verbindungen;  
  }
  
  public boolean[][] getVerbindungen()
  {
    return this.verbindungen;
  }
  
  public void setVerbindungen(boolean[][] v)
  {
    this.verbindungen = v;
  }
  
  public void setKantenProKnoten(int[] kantenProKnoten)
  {
    this.kantenProKnoten = kantenProKnoten;
  }
  
  public int getKnoten()
  {
    return this.nKnoten;  
  }
  
  public void setKnoten(int n)
  {
    this.nKnoten = n;
  }
  
  public int getKanten()
  {
    return this.nKanten;  
  }
  
  public void setKanten(int n)
  {
    this.nKanten = n;
  }
  
  public int getKantenProKnoten(int i)
  {
    return this.kantenProKnoten[i];
  }
  
  public void decrementKantenProKnoten(int i)
  {
    this.kantenProKnoten[i]--;
  }
  
  public boolean isVerbunden(int i, int j)
  {
    return this.verbindungen[i][j];
  }
  
  public void streicheVerknuepfung(int i, int j)
  {
    this.verbindungen[i][j] = false;
    this.verbindungen[j][i] = false;
  }
}
