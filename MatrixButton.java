import javafx.scene.control.Button;

public class MatrixButton extends Button
{
  private int row;
  private int column;
  private MatrixButton partner_button;
  
  public MatrixButton(int row, int column)
  {
    this.row = row;
    this.column = column;  
  }
  
  public int getRow()
  {
    return this.row;
  }
  
  public int getColumn()
  {
    return this.column;
  }
  
  public void setPartnerButton(MatrixButton mb)
  {
    this.partner_button = mb;
  }
  
  public MatrixButton getPartnerButton()
  {
    return this.partner_button;
  }  
}
