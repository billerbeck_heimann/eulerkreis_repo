import java.util.ArrayList;

public class Controller
{
  private Verknuepfung verknuepfung;
  private Output view;
  
  public Controller(Verknuepfung verknuepfung, Output view)
  {
    this.verknuepfung = verknuepfung;
    this.view = view;
  }
  
  public void setKnoten(int knots)
  {
    verknuepfung.setKnoten(knots);
  }
  
  public void setVerbindungen(boolean[][] verbindung)
  {
    int anz_knoten = verknuepfung.getKnoten();
    int anz_kanten = 0;
    int kantenProKnoten[] = new int[anz_knoten];
    
    for (int f = 0; f < anz_knoten; f++) {
      for (int g = 0; g < f; g++ ) {
        if (verbindung[f][g])
        {
          anz_kanten++;
        }
      } // end of for
    } // end of for
    
    for (int f = 0; f < anz_knoten; f++) {
      for (int g = 0; g < anz_knoten; g++ ) {
        if (verbindung[f][g])
        {
          kantenProKnoten[f]++;
        }
      } // end of for
    } // end of for
    
    anz_knoten = anz_knoten / 2;                      // Durch 2, da alle doppelt gezaehlt wurden
    verknuepfung.setKantenProKnoten(kantenProKnoten);
    verknuepfung.setKanten(anz_kanten);
    verknuepfung.setVerbindungen(verbindung);
  }
  
  public int requestKnots()
  {
    return verknuepfung.getKnoten();
  }
  
  public boolean[][] requestConnections()
  {
    return verknuepfung.getVerbindungen();
  }
  
  public ArrayList<Integer> getPfad() throws Exception
  {
    try
    {
      return EulerKreisFinder.findePfad(verknuepfung);
    }
    catch(Exception e)
    {
      // Rethrow
      throw e;
    }
  }
  
  public Verknuepfung getVerknuepfung()
  {
    return this.verknuepfung;
  }
  
  public Output getOutput()
  {
    return this.view;
  }
  
  public void setOutput(Output view)
  {
    this.view = view;
  }
  
  public void setVerknuepfung(Verknuepfung v)
  {
    this.verknuepfung = v;
  }
 
}
