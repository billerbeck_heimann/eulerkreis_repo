public class Eulerkreis
{
    public static void main(String[] args)
  {
    Verknuepfung verknuepfung = new Verknuepfung(1, 1, null, null);
    Output view = new Output();
    Controller controller = new Controller(verknuepfung, view);
    
    view.startGUI(controller);
  }
}
