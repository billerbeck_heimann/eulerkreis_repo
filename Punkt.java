import javafx.scene.canvas.*;
import javafx.scene.paint.Color; 

/**
 *
 * Beschreibung
 * Diese Klasse wird fuer die Grafische Darstellung von einem Knotensystem verwendet.
 * Es ist eine Klasse um einen Punkt (Knoten) darzustellen und die Position abzuspeichern.
 *
 * @version 1.0 vom 26.01.2022
 * @Jannik Heimann 
 */

public class Punkt
{
  // Attribute
  private double x;
  private double y;
  private double r;
  
  // Methoden
  public Punkt(double x, double y, double r)
  {
    this.x = x;
    this.y = y;
    this.r = r;
  }
  
  public void print(GraphicsContext gc)
  {
    gc.setFill(Color.BLUE);
    double p_off = this.r / 2;
    gc.fillOval(this.x - p_off, this.y - p_off, this.r, this.r);
  }
  
  public double getX()
  {
    return this.x;
  }
  
  public void setX(double x)
  {
    this.x = x;
  }
  
  public String toString()
  {
    return "X-Koordinate = " + this.x + "\nY-Koordinate = " + this.y + "\nRadius = " + this.r + "\n";
  }
  
    public double getY()
  {
    return this.y;
  }
  
  public void setY(double y)
  {
    this.y = y;
  }
  
    public double getR()
  {
    return this.r;
  }
  
  public void setR(double r)
  {
    this.r = r;
  }
}
